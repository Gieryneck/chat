const path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var webpack = require('webpack');

var UglifyJSPlugin = require('uglifyjs-webpack-plugin');
var OptimizeJsPlugin = require('optimize-js-plugin');
var env = process.env.NODE_ENV || 'development';

var plugins = [

    new HtmlWebpackPlugin({
        template: './index.html',
        filename: 'index.html',
        inject: 'body',
    })
];

if (env === 'production') {

plugins.push(
    new webpack.optimize.UglifyJsPlugin(),
    new OptimizeJsPlugin({
      sourceMap: false
    })
  );
}

console.log('NODE_ENV:', env);

module.exports = {

    devtool: 'eval-source-map',

    entry: (env !== 'production' ? [
        'react-hot-loader/patch',
        'webpack-dev-server/client?http://localhost:8080',
        'webpack/hot/only-dev-server',
    ] : []).concat(['./client/index.js']),

    output: {
        path: path.resolve(__dirname, "public"),
        filename: './bundle.js'
    },

    // loaders
    module: {

        rules: [

            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react']
                }
            },

            {
                test: /\.sass$/,
                use: [
                    
                    { loader: 'style-loader'},

                    {
                        loader: 'css-loader',
                        options: {

                            modules: true
                        }
                    },

                    { loader: 'sass-loader',
                        options: {

                            modules: true
                        }
                    }
                ]
            }
        ]
    },

    plugins: plugins
}
